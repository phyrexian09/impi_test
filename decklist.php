<?php include 'layout/header.php'; ?>

    <div class="row" style="margin-top: 230px; margin-bottom: 70px;">
        <div class="col-md-8 col-md-offset-2 text-center">
            <div class="btn-group" role="group">
                <a href="#" type="button" class="btn btn-default">Assassin</a>
                <a href="#" type="button" class="btn btn-default">Guerrier</a>
                <a href="#" type="button" class="btn btn-default">Spécialiste</a>
                <a href="#" type="button" class="btn btn-default">Soutien</a>
                <a href="#" type="button" class="btn btn-default">Spé. Impish</a>
            </div>
        </div>
    </div>

    <div class="row" style="margin-bottom: 70px;">
        <div class="col-md-8 col-md-offset-2 text-center">
            <div class="col-xs-2">
                <a href="#"><img src="./img/ada.png" class="img-responsive2" alt="Abathur" /><p>Abathur</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/anub.png" class="img-responsive2" alt="Anub’arak" /><p>Anub’arak</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/artanis.png" class="img-responsive2" alt="Artanis" /><p>Artanis</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/arthas.png" class="img-responsive2" alt="Arthas" /><p>Arthas</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/azmo.png" class="img-responsive2" alt="Azmodan" /><p>Azmodan</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/bright.png" class="img-responsive2" alt="Brightwing" /><p>Brightwing</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/butcher.png" class="img-responsive2" alt="Butcher" /><p>Butcher</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/chen.png" class="img-responsive2" alt="Chen" /><p>Chen</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/cho.png" class="img-responsive2" alt="Cho" /><p>Cho</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/diablo.png" class="img-responsive2" alt="Diablo" /><p>Diablo</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/etc.png" class="img-responsive2" alt="E.T.C." /><p>E.T.C.</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/falstad.png" class="img-responsive2" alt="Falstad" /><p>Falstad</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/gall.png" class="img-responsive2" alt="Gall" /><p>Gall</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/gaz.png" class="img-responsive2" alt="Gazlowe" /><p>Gazlowe</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/hammer.png" class="img-responsive2" alt="Sgt. Hammer" /><p>Hammer</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/illi.png" class="img-responsive2" alt="Illidan" /><p>Illidan</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/jaina.png" class="img-responsive2" alt="Jaina" /><p>Jaina</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/joha.png" class="img-responsive2" alt="Johanna" /><p>Johanna@</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/kael.png" class="img-responsive2" alt="Kael’thas" /><p>Kael’thas</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/kerri.png" class="img-responsive2" alt="Kerrigan" /><p>Kerrigan</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/khara.png" class="img-responsive2" alt="Kharazim" /><p>Kharazim</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/leo.png" class="img-responsive2" alt="Leoric" /><p>Leoric</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/lili.png" class="img-responsive2" alt="Li Li" /><p>Li Li</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/malf.png" class="img-responsive2" alt="Malfurion" /><p>Malfurion</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/mora.png" class="img-responsive2" alt="Lt.Morales" /><p>Lt.Morales</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/mura.png" class="img-responsive2" alt="Muradin" /><p>Muradin</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/murky.png" class="img-responsive2" alt="Murky" /><p>Murky</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/nazee.png" class="img-responsive2" alt="Nazeebo" /><p>Nazeebo</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/nova.png" class="img-responsive2" alt="Nova" /><p>Nova</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/raynor.png" class="img-responsive2" alt="Raynor" /><p>Raynor</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/rehgar.png" class="img-responsive2" alt="Rehgar" /><p>Rehgar</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/rexxar.png" class="img-responsive2" alt="Rexxar" /><p>Rexxar</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/sonya.png" class="img-responsive2" alt="Sonya" /><p>Sonya</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="./img/sti.png" class="img-responsive2" alt="Stitches" /><p>Stitches</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/sylvana.png" class="img-responsive2" alt="Sylvanas" /><p>Sylvanas</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/tassa.png" class="img-responsive2" alt="Tassadar" /><p>Tassadar</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/thrall.png" class="img-responsive2" alt="Thrall" /><p>Thrall</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/tychus.png" class="img-responsive2" alt="Tychus" /><p>Tychus</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/tyra.png" class="img-responsive2" alt="Tyrande" /><p>Tyrande</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/tyrael.png" class="img-responsive2" alt="Tyrael" /><p>Tyrael</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/uther.png" class="img-responsive2" alt="Uther" /><p>Uther</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/valla.png" class="img-responsive2" alt="Valla" /><p>Valla</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/viki.png" class="img-responsive2" alt="Viking" /><p>Viking</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/zagara.png" class="img-responsive2" alt="Zagara" /><p>Zagara</p></a>
            </div>
            <div class="col-xs-2">
                <a href="#"><img src="img/zera.png" class="img-responsive2" alt="Zeratul" /><p>Zeratul</p></a>
            </div>
        </div>
    </div>

<?php include 'layout/footer.php'; ?>