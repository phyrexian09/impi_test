"use strict";

// recupération sous forme de tableau de tous les boutons et les images de la page
var btn = document.querySelectorAll('.btn-default');
var img = document.querySelectorAll('.img-responsive2');

// gestion de la boucle de masquage des images nom désiré
function loop(className) {
    for (var i = 0; i < img.length; i++){
        var im = img[i];
        if(im.classList[1] !== className){
            im.parentNode.parentNode.classList.toggle('hide');
        }
    }
}

// gestion de l'evenement sur les images
function event(b) {
    b.addEventListener('click', function (e) {
        e.preventDefault();
        b.classList.toggle('active');
        // switch case en fonction des class d'image
        switch (b.getAttribute('id')){
            case 'Assassin':
                loop('Assassin');
                break;
            case 'Guerrier':
                loop('Guerrier');
                break;
            case 'Specialiste':
                loop('Specialiste');
                break;
            case 'Soutien':
                loop('Soutien');
                break;
            case 'Impish':
                loop('Impish');
                break;
        }

    });
}

// boucle d'evenement du click sur les boutons
for (var i = 0; i < btn.length; i++) {
    var b = btn[i];
    event(b);
}