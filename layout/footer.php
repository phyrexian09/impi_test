    <footer class="footer">
        <div class="row">
            <p class="col-xs-12">
                <span class="col-xs-6">Impishou TV - Copyright 2015. Tous Droits Réservés.</span>
                <span class="col-xs-6">
                    <a href="mailto:impishou.moba@gmail.com"><i class="fa fa-envelope-o"></i>&nbsp;Email&nbsp;|&nbsp;</a>
                    <a href="https://twitter.com/Impishou"><i class="fa fa-twitter"></i>&nbsp;Twitter&nbsp;|&nbsp;</a>
                    <a href="https://www.youtube.com/channel/UCOEx14xDMtyzN4XvPWV9HFw"><i class="fa fa-youtube"></i>&nbsp;Youtube&nbsp;|&nbsp;</a>
                    <a href="http://www.twitch.tv/impishou_tv/profile"><i class="fa fa-twitch"></i>&nbsp;Twitch</a>
                </span>
            </p>
        </div>
    </footer>

</div>

</body>
</html>