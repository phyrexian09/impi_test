<!DOCTYPE HTML>
<html lang = "en">
<head>
    <meta charset="utf-8" />
    <title>Impishou TV | Le stream au poil</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="./img/icon.png">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/main-stylesheet.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Oswald:300,400,700|Source+Sans+Pro:300,400,600,700&amp;subset=latin,latin-ext" />
    <!--[if lt IE 9 ]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<!--<body class="no-slider">-->
<body>

<div class="nav-address navbar-static-top">
    <div class="container">
        <div class="row">
            <p>
                <a href="mailto:impishou.moba@gmail.com"><i class="fa fa-envelope-o"></i>&nbsp;Email&nbsp;|&nbsp;</a>
                <a href="https://twitter.com/Impishou"><i class="fa fa-twitter"></i>&nbsp;Twitter&nbsp;|&nbsp;</a>
                <a href="https://www.youtube.com/channel/UCOEx14xDMtyzN4XvPWV9HFw"><i class="fa fa-youtube"></i>&nbsp;Youtube&nbsp;|&nbsp;</a>
                <a href="http://www.twitch.tv/impishou_tv/profile"><i class="fa fa-twitch"></i>&nbsp;Twitch</a>
            </p>
        </div>
    </div>
</div>

<div class="container">

    <header id="header">
        <div id="menu-bottom">
            <!-- <nav id="menu" class="main-menu width-fluid"> -->
            <nav id="menu" class="main-menu">
                <a href="index.php" class="header-logo left"><img src="./img/logo.png" alt="Impishou TV" /></a>
                <ul class="pull-right margin-header">
                    <li>
                        <a href="index.php"><span><i class="fa fa-caret-square-o-right"></i><strong>Le Stream</strong></span></a>
                    </li>
                    <li>
                        <a href="#"><span><i class="fa fa-commenting-o"></i><strong>Présentation</strong></span></a>
                        <ul class="sub-menu">
                            <li><a href="#">Qui est Impishou ?</a></li>
                            <li><a href="#">Qu'est ce qu'un Impish</a></li>
                            <li><a href="#">Comment rejoindre la secte ?</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="planning.php"><span><i class="fa fa-calendar-o"></i><strong>Planning</strong></span></a>
                        <ul class="sub-menu">
                            <li><a href="planning.php">Calendrier</a></li>
                            <li><a href="event.php">Évènements</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="decklist.php"><span><i class="fa fa-list-alt"></i><strong>Deck List</strong></span></a>
                        <ul class="sub-menu">
                            <li><a href="#">Assassin</a></li>
                            <li><a href="#">Guerrier</a></li>
                            <li><a href="#">Support</a></li>
                            <li><a href="#">Spécialiste</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><span><i class="fa fa-paper-plane-o"></i><strong>Contact</strong></span></a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>